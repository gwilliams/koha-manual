# Compendium of pt_BR.
msgid ""
msgstr ""
"Project-Id-Version: compendium-pt_BR\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-05-15 19:31-0300\n"
"PO-Revision-Date: 2018-05-15 19:52-0300\n"
"Language-Team: Koha Translation Team \n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../source/index.rst:3
#, fuzzy
msgid "Koha 18.05 Manual (en)"
msgstr "Manual do Koha %s"

#: ../../source/index.rst
msgid "Author"
msgstr "Autor"

#: ../../source/index.rst:5
#, fuzzy
msgid "The Koha Community"
msgstr "Tipo de unidade"
